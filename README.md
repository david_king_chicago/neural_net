# Axon

Axon is a C++ library for creating neural networks.

More details on the theory of neural networks can be found in these sources:

[A Neural Network in 11 lines of Python](http://iamtrask.github.io/2015/07/12/basic-python-network/)

[Neural Networks and Deep Learning](http://neuralnetworksanddeeplearning.com/index.html)

The Axon API supports:

* Building a network by composing sequential layers of interconnected neurons

* Initializing the weights of each connection

* Training the network to classify input samples

* Running the trained network against sample data


## Performance

To test Axon, I built a small network and trained it to classify hand-written digits.
I ran 100 training iterations over the [MNIST digit data set](http://yann.lecun.com/exdb/mnist/),
consisting of 60,000 labelled sample images. I then tasked the network to classify 10,000 test images
that were not part of the original training set. Each image is a 28 x 28 grayscale scanned image of a
hand-drawn number, zero through nine.

The network classified 90% of the test images correctly. The top ten best matches and bottom
ten worst matches are shown below. The columns labelled 0 - 9 represent the network's
relative "belief" that the source image is the specified digit.

The best matches show a near certain belief in a single digit and it matches the label.

The worst matches are when it has a strong incorrect belief, or
it is split between multiple incorrect guesses. I found it interesting to look at the
misclassifications. These seem like mistakes that a human could also make,
or at least it is easy to see how the network got confused.


### Best matches

 Image | Label | Error | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 |
-------|-------|-------|---|---|---|---|---|---|---|---|---|---|
![Best match 0](results/bestMatch0.png) |7 |0.00000 |0.000 |0.000 |0.000 |0.000 |0.000 |0.000 |0.000 |1.000 |0.000 |0.000 |
![Best match 1](results/bestMatch1.png) |4 |0.00001 |0.000 |0.000 |0.000 |0.001 |0.998 |0.000 |0.001 |0.001 |0.000 |0.002 |
![Best match 2](results/bestMatch2.png) |1 |0.00002 |0.000 |0.998 |0.001 |0.002 |0.000 |0.002 |0.000 |0.002 |0.000 |0.000 |
![Best match 3](results/bestMatch3.png) |3 |0.00003 |0.001 |0.000 |0.000 |0.997 |0.000 |0.002 |0.000 |0.001 |0.003 |0.000 |
![Best match 4](results/bestMatch4.png) |2 |0.00003 |0.003 |0.000 |0.999 |0.002 |0.002 |0.000 |0.001 |0.001 |0.004 |0.000 |
![Best match 5](results/bestMatch5.png) |5 |0.00007 |0.008 |0.002 |0.000 |0.001 |0.001 |0.998 |0.002 |0.000 |0.002 |0.000 |
![Best match 6](results/bestMatch6.png) |6 |0.00013 |0.003 |0.000 |0.004 |0.000 |0.009 |0.000 |0.997 |0.000 |0.000 |0.000 |
![Best match 7](results/bestMatch7.png) |0 |0.00013 |0.992 |0.000 |0.000 |0.000 |0.000 |0.008 |0.001 |0.001 |0.000 |0.001 |
![Best match 8](results/bestMatch8.png) |8 |0.00031 |0.002 |0.001 |0.000 |0.000 |0.005 |0.000 |0.009 |0.000 |0.988 |0.007 |
![Best match 9](results/bestMatch9.png) |9 |0.00049 |0.000 |0.000 |0.002 |0.000 |0.015 |0.000 |0.000 |0.002 |0.002 |0.984 |

### Worst matches

 Image | Label | Error | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 |
-------|-------|-------|---|---|---|---|---|---|---|---|---|---|
![Worst match 0](results/worstMatch0.png) |3 |2.12622 |0.000 |0.001 |0.000 |0.001 |0.023 |0.603 |0.022 |0.000 |0.874 |0.001 |
![Worst match 1](results/worstMatch1.png) |6 |2.08976 |0.000 |0.002 |0.000 |0.006 |0.005 |0.626 |0.011 |0.000 |0.849 |0.009 |
![Worst match 2](results/worstMatch2.png) |7 |2.01975 |0.045 |0.000 |0.943 |0.000 |0.001 |0.000 |0.005 |0.001 |0.361 |0.001 |
![Worst match 3](results/worstMatch3.png) |9 |1.99315 |0.000 |0.000 |0.000 |0.008 |0.000 |0.000 |0.000 |0.997 |0.000 |0.001 |
![Worst match 4](results/worstMatch4.png) |3 |1.99239 |0.008 |0.000 |0.000 |0.000 |0.001 |0.000 |0.000 |0.996 |0.000 |0.023 |
![Worst match 5](results/worstMatch5.png) |7 |1.99153 |0.000 |0.004 |0.997 |0.030 |0.000 |0.000 |0.009 |0.001 |0.000 |0.000 |
![Worst match 6](results/worstMatch6.png) |4 |1.98761 |0.000 |0.041 |0.000 |0.001 |0.004 |0.080 |0.001 |0.000 |0.994 |0.001 |
![Worst match 7](results/worstMatch7.png) |8 |1.98730 |0.000 |0.001 |0.003 |0.004 |0.000 |0.001 |0.000 |0.994 |0.000 |0.007 |
![Worst match 8](results/worstMatch8.png) |6 |1.98715 |0.000 |0.997 |0.040 |0.000 |0.001 |0.001 |0.004 |0.000 |0.000 |0.000 |
![Worst match 9](results/worstMatch9.png) |2 |1.98562 |0.000 |0.004 |0.000 |0.000 |0.993 |0.000 |0.004 |0.001 |0.000 |0.004 |

### Directory structure

`src/` The primary source code

`test/` Google Test unit tests for the code in src

`data/` The MNIST data set

`results/` The output from NeuralNetworkTrainer, including the best and worst images shown above

`CMakeLists.txt` The top level build files

### How to build it

Get it:

    git clone git@bitbucket.org:david_king_chicago/neural_net.git

You will need cmake version 3.5 or higher

The project depends on Boost and the Google Test framework.

Modify `neural_net/CMakeLists.txt` to set BOOST_ROOT and BOOST_LIBRARYDIR to point to your installation.
    
Installing Google Test on Linux is described here:

[How to setup Google Test](http://stackoverflow.com/questions/13513905/how-to-setup-googletest-as-a-shared-library-on-linux)

To perform a Debug build:

    cd neural_net
    mkdir cmake-build-debug
    cd cmake-build-debug
    cmake -DCMAKE_BUILD_TYPE=Debug ..
    make

Similarly, to perform a Release build:

    cd neural_net
    mkdir cmake-build-debug
    cd cmake-build-debug
    cmake -DCMAKE_BUILD_TYPE=Debug ..
    make

The generated executables are:

* **NeuralNetworkTrainer** - builds a simple network and trains it against the MNIST data set

* **MatrixTest** and **LayerTest** - Google Test scaffolds for the Matrix and Layer classes

### Class Overview

File            | Class             | Description
----------------|-------------------|----------------------------
Image.h         | Image             | An image represented as a matrix of grayscale values
Image.h         | ImageReader       | Iterates over the images in a MNIST data file
Label.h         | LabelReader       | Iterates over the labels in a MNIST data file
Layer.h         | Layer             | One layer in a neural network
Matrix.h        | Matrix            | A generic matrix class
MiniBatch.h     | MiniBatch         | A batch of training data that is processed as a unit by the NeuralNetwork during gradient descent
MiniBatch.h     | MiniBatchReader   | Iterates over a set of sample data and groups the data into MiniBatches
NeuralNetwork.h | NeuralNetwork     | The API for constructing and training a network
ResultsReport.h | ResultsReport     | Generates the data in the Performance section of this document
Sigmoid.h       | Sigmoid           | The default activation function used by the NeuralNetwork and Layer classes
Sigmoid.h       | SigmoidDerivative | The derivative of the Sigmoid function (used during back-propagation)


**NeuralNetworkTrainer.cpp** is the main program which trains a network on the MNIST data set.
It is a good place to start to understand the code.

### Tools and concepts I leveraged from the Advanced C++ class. Plus lessons learned.

I created a generic `Matrix` class similar, in concept, to the one in class, but I wrote this from scratch.
I only implemented the functionality I needed, but it turned out I needed quite a lot of features.
The decision to represent the elements of the matrix as a single vector of elements made a few things harder
(like calculating the element at a given row and column), but it allowed me to apply standard library
algorithms to all elements it once, which proved quite powerful. By convention, I chose to make
most operations return copies of the resulting matrix rather than performing in-place updates. This may
have resulted in more copies of matrixes being generated, but it felt safer (avoiding inadvertent
updates). I believe many of the copies would have been elided by the compiler but I need to
do more testing to verify this.

All classes that use matrices, such as NeuralNetwork, Layer, and Image are templated on MatrixType.
The goal is to support easily swapping the hand-rolled Matrix class with an industrial strength
library such as Eigen by creating an EigenMatrix that supports the same interface methods as
Matrix. I haven't tried this yet. I focused on correctness in this first iteration.

I leverage templates in every class. In the NeuralNetwok class, for example,
it's possible to swap in an entirely different activation function just by changing the template
parameters.

I leveraged the standard library algorithms *everywhere*.

I implemented custom input iterators (ImageReader, LabelReader, and MiniBatchReader) using
boost::function_input_iterator.

I used RAII and smart pointers to manage memory. For example, NeuralNetwork "owns" a unique pointer
to the first Layer. Subsequent Layers are chained together, with each layer owning the unique pointer
to the subsequent layer.

Having come from a primarily Java background I was surprised at how useful it was to
put instances of user-built types on the stack.

Moreover, building this C++ project, somewhat larger in scale than most of the class exercises,
forced me to integrate and apply many of the C++ concepts that I had previously focused on in
isolation. Overall it was a rewarding and fun experience. 












