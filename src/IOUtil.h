#pragma once

#include <cstdint>
#include <istream>
#include <netinet/in.h>

namespace neural {

    inline uint32_t readUint32(std::istream &input) {
        uint32_t valueBigEndian;
        input.read(reinterpret_cast<char *>(&valueBigEndian), 4);
        return ntohl(valueBigEndian);
    }

}


