#pragma once

#include <cmath>

namespace neural {
    struct SigmoidDerivative {
        // The derivative of the sigmoid function can be written in terms
        // of the sigmoid function itself:
        //
        //     Derivative { Sigmoid(x) } = Sigmoid(x) ( 1 - Sigmoid(x) )
        //
        // So, to avoid unnecessarily recomputing Sigmoid(x),
        // the input to the derivative function is not x, but Sigmoid(x)
        //
        double operator()(double sigmoidOutput) const {
            return sigmoidOutput * (1 - sigmoidOutput);
        }
    };

    struct Sigmoid {

        double operator()(double input) const {
            return 1 / (1 + ::exp(-1 * input));
        }

    };

}