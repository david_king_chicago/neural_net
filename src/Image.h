#pragma once

#include <istream>
#include <iostream>
#include <cstdint>
#include <netinet/in.h>
#include <boost/iterator/function_input_iterator.hpp>
#include "Matrix.h"
#include "IOUtil.h"
#include "Label.h"


namespace neural {

    template <typename MatrixType = Matrix<double>>
    struct Image {

        inline friend
        std::ostream &operator<<(std::ostream &output, Image const &image) {
            output << "Label: " << image.label << std::endl;
            output << "Data: " << image.matrix << std::endl;
            return output;
        }

        void setLabel(unsigned int labelValue) {
            label = labelValue;
            targetOutput.resize(1, 10);
            targetOutput(0, label) = 1.0;
        }

        bool actualOutputMatchesTarget() const {
            return (actualOutput.maxElementInRow(0) == label);
        }
        
        double error() const {
            MatrixType diff = (actualOutput - targetOutput);
            double result = 0.0;
            for (int i = 0; i < diff.columns(); i++) {
                result += (diff(0,i)*diff(0,i));
            }
            return result;
        }

        MatrixType matrix;
        MatrixType singleRow;
        MatrixType actualOutput;
        MatrixType targetOutput;
        unsigned int label;
    };

    // Reads images from a MNIST image file. See http://yann.lecun.com/exdb/mnist/
    template <typename MatrixType>
    class ImageReader {
    public:
        using result_type = Image<MatrixType>;
        using iterator = boost::function_input_iterator<ImageReader, std::size_t>;

        ImageReader(std::istream &input) : input{input} {
            input.seekg(4, std::ios::beg);
            numberOfImages = readUint32(input);
            rowCount = readUint32(input);
            columnCount = readUint32(input);
            elementSize = rowCount * columnCount;
            buffer = std::make_unique<char[]>(elementSize);
            image.matrix.resize(rowCount, columnCount);
            image.singleRow.resize(1, rowCount * columnCount);
        }

        result_type operator()() {
            input.read(buffer.get(), elementSize);
            for (int i = 0; i < image.matrix.rows(); i++) {
                for (int j = 0; j < image.matrix.columns(); j++) {
                    image.matrix(i, j) = static_cast<unsigned char>(buffer.get()[image.matrix.columns() * i + j]);
                }
            }
            for (int k = 0; k < image.singleRow.columns(); k++) {
                image.singleRow(0, k) = static_cast<unsigned char>(buffer.get()[k]);
            }
            return image;
        }

        iterator begin() {
            return boost::make_function_input_iterator(*this, static_cast<std::size_t>(0));
        }

        iterator end() {
            return boost::make_function_input_iterator(*this, numberOfImages);
        }

        std::size_t size() const {
            return numberOfImages;
        }

        std::size_t rows() const {
            return rowCount;
        }

        std::size_t columns() const {
            return columnCount;
        }

    private:
        std::istream &input;
        std::size_t numberOfImages;
        std::size_t elementSize;
        std::size_t rowCount;
        std::size_t columnCount;
        Image<MatrixType> image;
        std::unique_ptr<char[]> buffer;
    };

    template <typename MatrixType = Matrix<double> >
    std::vector<Image<MatrixType>>
    readImages(boost::filesystem::path const& imageFile, boost::filesystem::path const& labelFile) {
        // Read image file
        std::ifstream imageStream{imageFile.string(), std::ios::in | std::ios::binary };
        ImageReader<Matrix<double>> imageReader{imageStream};
        std::vector<Image<>> images(imageReader.size());
        std::copy(imageReader.begin(), imageReader.end(), images.begin());

        // Read labels
        std::ifstream labelStream{labelFile.string(), std::ios::in | std::ios::binary };
        LabelReader<unsigned int> labelReader{labelStream};
        std::vector<unsigned int> labels(labelReader.size());
        std::copy(labelReader.begin(), labelReader.end(), labels.begin());

        assert(images.size() == labels.size());

        for (int i = 0; i < images.size(); i++) {
            images[i].setLabel(labels[i]);
        }

        return images;
    }


    void writePgmFile(boost::filesystem::path const& imageFile, Image<> const& image) {
        std::ofstream out{imageFile.string()};
        out << "P2" << std::endl;
        out << "# Label: " << image.label << std::endl;
        out << image.matrix.columns() << " " << image.matrix.rows() << std::endl;
        out << "255" << std::endl;
        for (int i = 0; i < image.matrix.rows(); i++) {
            for (int j = 0; j < image.matrix.columns(); j++) {
                out << std::setw(4) << image.matrix(i,j);
            }
            out << std::endl;
        }
    }

}