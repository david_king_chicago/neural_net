#pragma once

#include "Sigmoid.h"
#include "Matrix.h"
#include "Layer.h"

namespace neural {

    template<
            typename SampleType,
            typename MatrixType = Matrix<double>,
            typename ActivationFunction = Sigmoid,
            typename ActivationFunctionDerivative = SigmoidDerivative>
    class NeuralNetwork {
    public:
        using size_type = typename MatrixType::size_type;
        using LayerType = typename neural::Layer<MatrixType, ActivationFunction, ActivationFunctionDerivative>;

        NeuralNetwork(double learningRate) : learningRate{learningRate}, firstLayer{nullptr} {}

        void addLayer(MatrixType const& weights, MatrixType const& biases) {
            if (firstLayer) {
                firstLayer->addLayer(weights, biases);
            } else {
                firstLayer = std::make_unique<LayerType>(learningRate, weights, biases);
            }
        }

        template<typename RandomEngine>
        void trainAndEvaluate(
                std::vector<SampleType>& trainingDataSet,
                std::vector<SampleType>& testDataSet,
                int numberOfIterations,
                std::size_t samplesPerMiniBatch,
                std::size_t miniBatchInputColumns,
                std::size_t miniBatchTargetColumns,
                RandomEngine& randomEngine,
                std::ostream& progress_log) {

            std::size_t numberOfTrainingMiniBatches = trainingDataSet.size() / samplesPerMiniBatch;

            for (int i = 0; i < numberOfIterations; i++) {
                std::shuffle(trainingDataSet.begin(), trainingDataSet.end(), randomEngine);

                MiniBatchReader<MatrixType, typename std::vector<SampleType>::const_iterator> trainingMiniBatchReader{
                        trainingDataSet.begin(), samplesPerMiniBatch, miniBatchInputColumns, miniBatchTargetColumns,
                        numberOfTrainingMiniBatches};

                for (auto& trainingMiniBatch : trainingMiniBatchReader) {
                    firstLayer->train(trainingMiniBatch);
                }

                int numberOfMatches = evaluate(testDataSet);

                progress_log << "Iteration " << (i + 1) << "/"
                             << numberOfIterations
                             << ":  matched " << numberOfMatches
                             << "/" << testDataSet.size()
                             << std::endl;
            }
        }

        int evaluate(std::vector<SampleType>& testDataSet) {
            int numberOfMatches = 0;

            for (auto& testData : testDataSet) {
                testData.actualOutput = firstLayer->feedForward(testData.singleRow);
                if (testData.actualOutputMatchesTarget()) {
                    numberOfMatches++;
                }
            }
            return numberOfMatches;
        }

    private:
        double learningRate;
        std::unique_ptr<LayerType> firstLayer;
    };
}