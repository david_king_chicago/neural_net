#pragma once

#include <cassert>
#include "Matrix.h"
#include "Sigmoid.h"
#include "MiniBatch.h"

namespace neural {

    template<
            typename MatrixType = Matrix<double>,
            typename ActivationFunction = Sigmoid,
            typename ActivationFunctionDerivative = SigmoidDerivative>
    class Layer {
    public:
        using size_type = typename MatrixType::size_type;

        Layer() : learningRate{1}, weights{}, biases{}, activationFunction{}, activationFunctionDerivative{} {}

        Layer(double learningRate, MatrixType const& weights, MatrixType const& biases) :
                learningRate{learningRate}, weights(weights), biases{biases}, activationFunction{},
                activationFunctionDerivative{} {
            assert(weights.columns() == biases.columns());
            assert(biases.rows() == 1);
        }

        void addLayer(MatrixType const& nextLayerWeights, MatrixType const& nextLayerBiases) {
            if (nextLayer) {
                nextLayer->addLayer(nextLayerWeights, nextLayerBiases);
            } else {
                assert(this->weights.columns() == nextLayerWeights.rows());
                assert(nextLayerWeights.columns() == nextLayerBiases.columns());
                assert(nextLayerBiases.rows() == 1);

                nextLayer = std::make_unique<Layer>(learningRate, nextLayerWeights, nextLayerBiases);
            }
        }

        MatrixType train(MiniBatch<MatrixType> const& miniBatch) {
            return train(miniBatch.input, miniBatch.targetOutput);
        }

        MatrixType train(MatrixType const& input, MatrixType const& targetOutput) {
            assert(input.rows() == targetOutput.rows());
            assert(input.columns() == weights.rows());
            assert(nextLayer || (targetOutput.columns() == weights.columns()));

            MatrixType output = feedForwardThisLayer(input);
            MatrixType errorForThisLayer = trainNextLayer(output, targetOutput);
            MatrixType outputDelta = schurProduct(errorForThisLayer, output.apply(activationFunctionDerivative));
            MatrixType errorForPreviousLayer = outputDelta * weights.transpose();
            adjustWeightsAndBiases(input, outputDelta);
            return errorForPreviousLayer;
        }

        MatrixType feedForward(MiniBatch<MatrixType> const& miniBatch) {
            return feedForward(miniBatch.input);
        }

        MatrixType feedForward(MatrixType const& input) {
            MatrixType output = feedForwardThisLayer(input);
            if (nextLayer) {
                return nextLayer->feedForward(output);
            } else {
                return output;
            }
        }

        MatrixType feedForwardThisLayer(MatrixType const& input) {
            return ((input * weights).addToEachRow(biases)).apply(activationFunction);
        }

        inline friend
        std::ostream& operator<<(std::ostream& output,
                                 Layer<MatrixType, ActivationFunction, ActivationFunctionDerivative> const& layer) {
            output << "weights = " << std::endl;
            output << layer.weights << std::endl;
            output << "biases = " << std::endl;
            output << layer.biases << std::endl;
            return output;
        }

    private:

        void adjustWeightsAndBiases(MatrixType const& input, MatrixType const& outputDelta) {
            weights = weights + learningRate * dotProduct(input, outputDelta);
            biases = biases + learningRate * outputDelta.sumOfRows();
        }

        MatrixType trainNextLayer(MatrixType const& output, MatrixType const& targetOutput) {
            if (nextLayer) {
                return nextLayer->train(output, targetOutput);
            } else {
                return (targetOutput - output);
            }
        }

        double learningRate;
        MatrixType weights;
        MatrixType biases;
        ActivationFunction activationFunction;
        ActivationFunctionDerivative activationFunctionDerivative;
        std::unique_ptr<Layer<MatrixType, ActivationFunction, ActivationFunctionDerivative>> nextLayer;
    };

}