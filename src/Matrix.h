#pragma once

#include <initializer_list>
#include <vector>
#include <stdexcept>
#include <algorithm>
#include <cassert>
#include <iomanip>

namespace neural {

    template<typename Scalar = double>
    class Matrix {
    public:
        using size_type = std::size_t;

        Matrix() : elements(), rowCount{0}, columnCount{0} {}

        Matrix(size_type rowCount, size_type columnCount)
                : elements(rowCount * columnCount), rowCount{rowCount}, columnCount{columnCount} {}

        void resize(size_type newRowCount, size_type newColumnCount) {
            rowCount = newRowCount;
            columnCount = newColumnCount;
            elements.resize(newRowCount * newColumnCount);
        }

        template<typename InitializerFunction>
        Matrix(size_type rowCount, size_type columnCount, InitializerFunction initializerFunction)
                : elements(rowCount * columnCount), rowCount{rowCount}, columnCount{columnCount} {
            std::transform(elements.begin(), elements.end(), elements.begin(),
                           [&initializerFunction](auto& element) { return initializerFunction(); });
        }

        Matrix(std::initializer_list<std::initializer_list<Scalar>> init) :
                rowCount{init.size()}, columnCount{init.size() == 0 ? 0 : init.begin()->size()} {
            elements.resize(rowCount * columnCount);
            auto elementIter = elements.begin();
            for (auto& row : init) {
                assert(row.size() == columnCount);
                elementIter = std::copy(row.begin(), row.end(), elementIter);
            }
        }

        Matrix(Matrix const& other) :
                elements{other.elements}, rowCount{other.rowCount}, columnCount{other.columnCount} {}

        Matrix(Matrix&& other) :
                elements{std::move(other.elements)}, rowCount{other.rowCount}, columnCount{other.columnCount} {}

        Matrix& operator=(Matrix&& other) {
            elements = std::move(other.elements);
            rowCount = other.rowCount;
            columnCount = other.columnCount;
            return *this;
        }

        Matrix& operator=(Matrix const& other) {
            elements = other.elements;
            rowCount = other.rowCount;
            columnCount = other.columnCount;
            return *this;
        }

        void setRow(int targetRowIndex, Matrix<Scalar> const& sourceMatrix, int sourceRowIndex) {
            assert(sourceMatrix.columns() == this->columns());
            size_type sourceRowStart = offset(sourceRowIndex, 0);
            size_type sourceRowEnd = sourceRowStart + sourceMatrix.columns();
            size_type targetRowStart = offset(targetRowIndex, 0);
            std::copy(sourceMatrix.elements.begin() + sourceRowStart, sourceMatrix.elements.begin() + sourceRowEnd,
                      elements.begin() + targetRowStart);
        }

        size_type rows() const { return rowCount; }

        size_type columns() const { return columnCount; }

        double normSquared() const {
            return std::accumulate(elements.begin(), elements.end(), 0,
                                   [](Scalar const& leftElement, Scalar const& rightElement) {
                                       return leftElement + rightElement*rightElement;
                                   });
        }

        Scalar operator()(int rowIndex, int columnIndex) const {
            return elements[offset(rowIndex, columnIndex)];
        }

        Scalar& operator()(int rowIndex, int columnIndex) {
            return elements[offset(rowIndex, columnIndex)];
        }

        Matrix<Scalar> transpose() const {
            Matrix<Scalar> result(columnCount, rowCount);
            for (int i = 0; i < rowCount; i++) {
                for (int j = 0; j < columnCount; j++) {
                    result(j, i) = (*this)(i, j);
                }
            }
            return result;
        }

        template<typename UnaryOperator>
        Matrix<Scalar> apply(UnaryOperator op) const {
            Matrix<Scalar> result(rowCount, columnCount);
            std::transform(elements.begin(), elements.end(), result.elements.begin(), op);
            return result;
        }

        // Adds rowVector to each row in this matrix, returning a copy
        // of the resulting matrix. The resulting matrix will have the
        // same dimensions as this matrix.
        Matrix<Scalar> addToEachRow(Matrix<Scalar> const& rowVector) const {
            assert(rowVector.rows() == 1);
            assert(rowVector.columns() == this->columns());
            Matrix<Scalar> result(rowCount, columnCount);
            auto elementIter = elements.begin();
            auto resultIter = result.elements.begin();
            for (int i = 0; i < rowCount; i++) {
                std::transform(rowVector.elements.begin(), rowVector.elements.end(), elementIter, resultIter,
                               [](Scalar const& leftElement, Scalar const& rightElement) {
                                   return leftElement + rightElement;
                               });
                std::advance(elementIter, columnCount);
                std::advance(resultIter, columnCount);
            }
            return result;
        }

        Matrix<Scalar> sumOfRows() const {
            Matrix<Scalar> result(1, columnCount);
            auto elementIter = elements.begin();
            for (int i = 0; i < rowCount; i++) {
                std::transform(result.elements.begin(), result.elements.end(), elementIter, result.elements.begin(),
                               [](const Scalar& leftElement, const Scalar& rightElement) {
                                   return leftElement + rightElement;
                               });
                std::advance(elementIter, columnCount);
            }
            return result;
        }

        Scalar sum() const {
            return std::accumulate(elements.begin(), elements.end(), 0);
        }

        Matrix<Scalar> normalize() const {
            Scalar sumOfElements = sum();
            Matrix<Scalar> result(rowCount, columnCount);
            for (int i = 0; i < columnCount; i++) {
                for (int j = 0; j < rowCount; j++) {
                    result(i,j) = elements[offset(i,j)] / sumOfElements;
                }
            }
            return result;
        }

        inline friend
        bool equalDimensions(Matrix<Scalar> const& left, Matrix<Scalar> const& right) {
            return ((left.rows() == right.rows()) && (left.columns() == right.columns()));
        }

        inline friend
        Matrix<Scalar> operator+(Matrix<Scalar> const& left, Matrix<Scalar> const& right) {
            assert(equalDimensions(left, right));
            Matrix<Scalar> result(left.rows(), left.columns());
            std::transform(left.elements.begin(), left.elements.end(), right.elements.begin(), result.elements.begin(),
                           [](const Scalar& leftElement, const Scalar& rightElement) {
                               return leftElement + rightElement;
                           });
            return result;
        }

        inline friend
        Matrix<Scalar> operator-(Matrix<Scalar> const& left, Matrix<Scalar> const& right) {
            return left + ((-1) * right);
        }

        inline friend
        Matrix<Scalar> operator*(const Scalar& scalar, Matrix<Scalar> const& matrix) {
            return matrix.apply([&scalar](const Scalar& element) { return scalar * element; });
        }

        inline friend
        Matrix<Scalar> operator*(Matrix<Scalar> const& matrix, const Scalar& scalar) {
            return scalar * matrix;
        }

        inline friend
        Matrix<Scalar> operator*(Matrix<Scalar> const& left, Matrix<Scalar> const& right) {
            assert(left.columns() == right.rows());
            Matrix<Scalar> result(left.rows(), right.columns());
            for (int i = 0; i < left.rows(); i++) {
                for (int j = 0; j < right.columns(); j++) {
                    Scalar total{0};
                    for (int k = 0; k < left.columns(); k++) {
                        total += left(i, k) * right(k, j);
                    }
                    result(i, j) = total;
                }
            }
            return result;
        }

        inline friend
        Matrix<Scalar> schurProduct(Matrix<Scalar> const& left, Matrix<Scalar> const& right) {
            assert(equalDimensions(left, right));
            Matrix<Scalar> result(left.rows(), left.columns());
            std::transform(left.elements.begin(), left.elements.end(), right.elements.begin(), result.elements.begin(),
                           [](const Scalar& leftElement, const Scalar& rightElement) {
                               return leftElement * rightElement;
                           });
            return result;
        }

        inline friend
        Matrix<Scalar> dotProduct(Matrix<Scalar> const& left, Matrix<Scalar> const& right) {
            return left.transpose() * right;
        }

        inline friend
        std::ostream& operator<<(std::ostream& output, Matrix<Scalar> const& matrix) {
            output << "{";
            for (int i = 0; i < matrix.rows(); i++) {
                if (i != 0) {
                    output << " ";
                }
                output << " { ";
                for (int j = 0; j < matrix.columns(); j++) {
                    if (j != 0) {
                        output << ", ";
                    }
                    output << std::setw(4) << matrix(i, j);
                }
                output << " }";
                if (i != (matrix.rows() - 1)) {
                    output << ", " << std::endl;
                }
            }
            output << " }";
            return output;
        }

        int maxElementInRow(int rowIndex) const {
            size_type rowStart = offset(rowIndex, 0);
            Scalar maxElement = elements[rowStart];
            int maxColumn = 0;
            for (int column = 0; column < columns(); column++) {
                if (elements[rowStart + column] > maxElement) {
                    maxElement = elements[rowStart + column];
                    maxColumn = column;
                }
            }
            return maxColumn;
        }

    private:
        size_type offset(int rowIndex, int columnIndex) const { return rowIndex * columnCount + columnIndex; }

        std::vector<Scalar> elements;
        size_type rowCount;
        size_type columnCount;
    };

}