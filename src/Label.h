#pragma once

#include <istream>
#include <boost/iterator/function_input_iterator.hpp>
#include "IOUtil.h"

namespace neural {

    // Reads labels from a MNIST label file. See http://yann.lecun.com/exdb/mnist/
    template <typename LabelType>
    class LabelReader {
    public:
        using result_type = LabelType;
        using iterator = boost::function_input_iterator<LabelReader, std::size_t>;

        LabelReader(std::istream& input) : input{input} {
            input.seekg(4, std::ios::beg);
            numberOfLabels = readUint32(input);
        }

        result_type operator() () {
            return static_cast<result_type>(input.get());
        }

        iterator begin() {
            return boost::make_function_input_iterator(*this, static_cast<std::size_t>(0));
        }

        iterator end() {
            return boost::make_function_input_iterator(*this, numberOfLabels);
        }

        size_t size() const {
            return numberOfLabels;
        }

    private:
        std::istream &input;
        size_t numberOfLabels;
    };
}