#include <boost/filesystem.hpp>
#include <iostream>
#include "Image.h"
#include "NeuralNetwork.h"
#include "ResultsReport.h"


using namespace boost::filesystem;
using namespace neural;

int main() {
    path dataDirectory = path{__FILE__}.parent_path().parent_path() / path{"data"};
    path trainingImageFile = dataDirectory / path{"train-images.idx3-ubyte"};
    path trainingLabelFile = dataDirectory / path{"train-labels.idx1-ubyte"};
    path testImageFile = dataDirectory / path{"t10k-images.idx3-ubyte"};
    path testLabelFile = dataDirectory / path{"t10k-labels.idx1-ubyte"};
    path resultsDirectory = path{__FILE__}.parent_path().parent_path() / path {"results"};

    int numberOfIterations = 100;
    std::size_t imagesPerMiniBatch = 10;
    std::size_t inputSize = 28 * 28;
    std::size_t hiddenLayerSize = 30;
    std::size_t outputSize = 10;
    double learningRate = 0.004;

    std::vector<Image<>> trainingImages = readImages(trainingImageFile, trainingLabelFile);
    std::vector<Image<>> testImages = readImages(testImageFile, testLabelFile);

    NeuralNetwork<Image<>> neuralNetwork{learningRate};

    std::mt19937 randomEngine{124L};
    std::normal_distribution<double> normalDistribution{0, 1};

    neuralNetwork.addLayer(
            Matrix<>(inputSize, hiddenLayerSize, [&] { return normalDistribution(randomEngine); }),
            Matrix<>(1, hiddenLayerSize, [&] { return normalDistribution(randomEngine); }));
    neuralNetwork.addLayer(
            Matrix<>(hiddenLayerSize, outputSize, [&] { return normalDistribution(randomEngine); }),
            Matrix<>(1, outputSize, [&] { return normalDistribution(randomEngine); }));

    neuralNetwork.trainAndEvaluate(trainingImages, testImages, numberOfIterations, imagesPerMiniBatch, inputSize,
                                   outputSize, randomEngine, std::cout);

    ResultsReport report{10, testImages, resultsDirectory};
    report.generate();

    return 0;
}
