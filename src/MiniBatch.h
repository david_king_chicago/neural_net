#pragma once

#include <boost/iterator/function_input_iterator.hpp>
#include <boost/filesystem.hpp>
#include <iostream>
#include "Matrix.h"
#include "Image.h"
#include "Label.h"

namespace neural {

    template<typename MatrixType = Matrix<double> >
    struct MiniBatch {
        using size_type = Matrix<>::size_type;

        MiniBatch() : input{}, targetOutput{}, actualOutput{} {}

        MiniBatch(size_type inputRows, size_type inputColumns, size_type targetColumns) :
                input(inputRows, inputColumns), targetOutput(inputRows, targetColumns),
                actualOutput(inputRows, targetColumns) {}

        MatrixType input;
        MatrixType targetOutput;
        MatrixType actualOutput;
    };

    template<typename MatrixType, typename ImageIterator>
    class MiniBatchReader {
    public:

        using result_type = MiniBatch<MatrixType>;

        MiniBatchReader(ImageIterator imageIterator,
                        std::size_t inputRows,
                        std::size_t inputColumns,
                        std::size_t targetColumns,
                        std::size_t numberOfMiniBatches
        ) :
                imageIterator{imageIterator}, inputRows{inputRows},
                inputColumns{inputColumns}, targetColumns{targetColumns},
                numberOfMiniBatches{numberOfMiniBatches} {}

        result_type operator()() {
            MiniBatch<MatrixType> miniBatch{inputRows, inputColumns, targetColumns};
            for (std::size_t inputRow = 0; inputRow < inputRows; inputRow++) {
                miniBatch.input.setRow(inputRow, (*imageIterator).singleRow, 0);
                miniBatch.targetOutput(inputRow, (*imageIterator).label) = 1;
                imageIterator++;
            }
            return miniBatch;
        }

        boost::function_input_iterator<MiniBatchReader, std::size_t> begin() {
            return boost::make_function_input_iterator(*this, static_cast<std::size_t>(0));
        }

        boost::function_input_iterator<MiniBatchReader, std::size_t> end() {
            return boost::make_function_input_iterator(*this, numberOfMiniBatches);
        }


    private:

        ImageIterator imageIterator;
        std::size_t inputRows;
        std::size_t inputColumns;
        std::size_t targetColumns;
        std::size_t numberOfMiniBatches;
    };


}
