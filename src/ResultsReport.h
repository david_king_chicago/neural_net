#include <boost/filesystem/path.hpp>
#include <boost/format.hpp>
#include <fstream>
#include "Image.h"


namespace neural {


    class ResultsReport {
    public:
        ResultsReport(std::size_t numberOfElements, std::vector<Image<>> const& testImages,
                      boost::filesystem::path const& resultsDirectory)
                : numberOfElements{numberOfElements}, testImages{testImages}, bestMatches{},
                  worstMatches(numberOfElements),
                  resultsDirectory{resultsDirectory} {}

        void generate() {
            collectAndWriteBestMatches();
            collectAndWriteWorstMatches();
            writeSummary();
        }

        void collectAndWriteBestMatches() {
            collectAndWriteBestMatches("bestMatch", [](Image<> const& left, Image<> const& right) {
                return left.error() < right.error();
            });
        }

        void collectAndWriteWorstMatches() {
            collectAndWriteWorstMatches("worstMatch", [](Image<> const& left, Image<> const& right) {
                return left.error() > right.error();
            });
        }

        template<typename CompareFunction>
        void collectAndWriteWorstMatches(std::string const& filePrefix,
                                         CompareFunction compareFunction) {
            std::partial_sort(testImages.begin(), testImages.begin() + numberOfElements, testImages.end(),
                              compareFunction);
            std::copy_n(testImages.begin(), numberOfElements, worstMatches.begin());
            for (int i = 0; i < worstMatches.size(); i++) {
                boost::filesystem::path imageFile =
                        resultsDirectory / boost::filesystem::path{filePrefix + std::to_string(i) + ".ppm"};
                writePgmFile(imageFile, worstMatches[i]);
            }
        }

        template<typename CompareFunction>
        void collectAndWriteBestMatches( std::string const& filePrefix,
                                         CompareFunction compareFunction) {
            std::sort(testImages.begin(), testImages.end(), compareFunction);
            for (int i = 0; i < 10; i++) {
                bestMatches.push_back(findBest(testImages, i));
            }
            std::sort(bestMatches.begin(), bestMatches.end(), compareFunction);
            for (int i = 0; i < bestMatches.size(); i++) {
                boost::filesystem::path imageFile =
                        resultsDirectory / boost::filesystem::path{filePrefix + std::to_string(i) + ".ppm"};
                writePgmFile(imageFile, bestMatches[i]);
            }
        }

        Image<> findBest(std::vector<Image<>>& images, unsigned int label) {
            std::vector<Image<>> filtered{};
            for (auto image : images) {
                if (image.label == label) {
                    filtered.push_back(image);
                }
            }
            return (*std::min_element(filtered.begin(), filtered.end(), [&](Image<> const& left, Image<> const& right){
                return left.error() < right.error();
            }));
        }


        void writeSummary() {
            boost::filesystem::path summaryFile = resultsDirectory / boost::filesystem::path{"results.md"};
            std::cout << "Writing: " << summaryFile.string() << std::endl;
            std::ofstream out{summaryFile.string()};
            out << "## Results" << std::endl;
            out << std::endl;

            out << "### Best matches" << std::endl;
            out << std::endl;
            out << " Image | Label | Error | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 |" << std::endl;
            out << "-------|-------|-------|---|---|---|---|---|---|---|---|---|---|" << std::endl;
            for (int i = 0; i < bestMatches.size(); i++) {
                out << "![Best match " << i << "](results/bestMatch" << i << ".png) |";
                out << bestMatches[i].label << " |";
                out << boost::format("%1.5f") % bestMatches[i].error() << " |";
                auto normalizedActualOutput = bestMatches[i].actualOutput;
                for (int j = 0; j < normalizedActualOutput.columns(); j++) {
                    out << boost::format("%1.3f") % normalizedActualOutput(0, j) << " |";
                }
                out << std::endl;
            }
            out << std::endl;

            out << "### Worst matches" << std::endl;
            out << std::endl;
            out << " Image | Label | Error | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 |" << std::endl;
            out << "-------|-------|-------|---|---|---|---|---|---|---|---|---|---|" << std::endl;
            for (int i = 0; i < worstMatches.size(); i++) {
                out << "![Worst match " << i << "](results/worstMatch" << i << ".png) |";
                out << worstMatches[i].label << " |";
                out << boost::format("%1.5f") % worstMatches[i].error() << " |";
                auto normalizedActualOutput = worstMatches[i].actualOutput;
                for (int j = 0; j < normalizedActualOutput.columns(); j++) {
                    out << boost::format("%1.3f") % normalizedActualOutput(0, j) << " |";
                }
                out << std::endl;
            }
            out << std::endl;
        }

    private:
        std::size_t numberOfElements;
        std::vector<Image<>> testImages;
        std::vector<Image<>> bestMatches;
        std::vector<Image<>> worstMatches;
        boost::filesystem::path resultsDirectory;
    };
}