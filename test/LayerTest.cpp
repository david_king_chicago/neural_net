#include <gtest/gtest.h>
#include <Layer.h>

namespace neural {
    class LayerTest : public ::testing::Test {
    protected:

        using size_type = typename Layer<>::size_type;

        std::mt19937 random{123L};
        std::normal_distribution<double> dist{0, 1};
        const double learningRate = 1.0;

        Layer<> layer;
        Matrix<> input;
        Matrix<> output;
        Matrix<> targetOutput;

        void givenALayerWithWeightsAndBiases(std::initializer_list<std::initializer_list<double>> weightsInit,
                                             std::initializer_list<std::initializer_list<double>> biasesInit) {
            this->layer = Layer<>(learningRate, Matrix<>(weightsInit), Matrix<>(biasesInit));
        }

        void givenALayerWithRandomWeights(size_type rows, size_type columns) {
            this->layer = Layer<>(learningRate,
                                  Matrix<>(rows, columns, [&] { return dist(random); }),
                                  Matrix<>(1, columns, [&] { return dist(random); })
            );
        }

        void givenInput(std::initializer_list<std::initializer_list<double>> inputInit) {
            this->input = Matrix<>(inputInit);
        }

        void givenTargetOutput(std::initializer_list<std::initializer_list<double>> targetOutputInit) {
            this->targetOutput = Matrix<>(targetOutputInit);
        }

        void whenLayerIsAddedWithRandomWeights(size_type rows, size_type columns) {
            this->layer.addLayer(
                    Matrix<>(rows, columns, [&] { return dist(random); }),
                    Matrix<>(1, columns, [&] { return dist(random); })
            );
        }

        void whenTrained(int iterations) {
            for (int i = 0; i < iterations; i++) {
                layer.train(input, targetOutput);
            }
        }

        void whenFedForward() {
            output = layer.feedForward(input);
        }

        void thenTotalErrorLessThan(double totalErrorUpperBound) {
            Matrix<> errorVector = (targetOutput - output);
            Matrix<> totalError = dotProduct(errorVector, errorVector);
            ASSERT_LT(totalError(0, 0), totalErrorUpperBound);
        }

        void thenDisplayError() {
            Matrix<> errorVector = (targetOutput - output);
            Matrix<> totalError = dotProduct(errorVector, errorVector);
            std::cout << "outputVector = " << std::endl;
            std::cout << output << std::endl;
            std::cout << "errorVector = " << std::endl;
            std::cout << errorVector << std::endl;
            std::cout << "totalError  = " << totalError << std::endl;
            std::cout << "-----------------------------------" << std::endl;
            std::cout << std::endl;
        }
    };

    TEST_F(LayerTest, ConstructALayer) {
        givenALayerWithWeightsAndBiases(
                {
                        {1},
                        {1},
                        {1}
                },
                {
                        {0.1}
                }
        );
    }

    TEST_F(LayerTest, TrainOneLayer) {
        givenALayerWithRandomWeights(3, 1);
        givenInput({
                           {0, 0, 1},
                           {0, 1, 1},
                           {1, 0, 1},
                           {1, 1, 1},
                   });
        givenTargetOutput({
                                  {0},
                                  {0},
                                  {1},
                                  {1}
                          });
        whenTrained(10'000);
        whenFedForward();
        thenTotalErrorLessThan(0.001);
        thenDisplayError();
    }

    TEST_F(LayerTest, TrainTwoLayers) {
        givenALayerWithRandomWeights(3, 4);
        whenLayerIsAddedWithRandomWeights(4, 1);
        givenInput({
                           {0, 0, 1},
                           {0, 1, 1},
                           {1, 0, 1},
                           {1, 1, 1},
                   });
        givenTargetOutput({
                                  {0},
                                  {1},
                                  {1},
                                  {0}
                          });
        whenTrained(100'000);
        whenFedForward();
        thenTotalErrorLessThan(0.0001);
        thenDisplayError();
    }
}

int main(int argc, char *argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}