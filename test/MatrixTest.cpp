#include <gtest/gtest.h>
#include <Matrix.h>

namespace neural {
    using Size = Matrix<double>::size_type;

    class MatrixTest : public ::testing::Test {
    protected:
        Matrix<double> matrix;
        Matrix<double> anotherMatrix;

        void givenAMatrix(std::initializer_list<std::initializer_list<double>> init) {
            matrix = init;
        }

        void givenAnotherMatrix(std::initializer_list<std::initializer_list<double>> init) {
            anotherMatrix = init;
        }

        void whenTransposed() {
            matrix = matrix.transpose();
        }

        void whenMultipliedBy(double scalar) {
            matrix = scalar * matrix;
        }

        void whenMultipliedOnRightBy(std::initializer_list<std::initializer_list<double>> m) {
            matrix = matrix * Matrix<double>(m);
        }

        void whenAdded(std::initializer_list<std::initializer_list<double>> m) {
            matrix = matrix + Matrix<double>(m);
        }

        void whenAddedToEachRow(std::initializer_list<std::initializer_list<double>> rowVector) {
            matrix = matrix.addToEachRow(rowVector);
        }

        void whenRowsAreSummed() {
            matrix = matrix.sumOfRows();
        }

        void whenSecondRowIsSet() {
            matrix.setRow(1, anotherMatrix, 0);
        }

        void whenNormalized() {
            matrix = matrix.normalize();
        }

        void thenDimensionsAre(Size rows, Size columns) {
            ASSERT_EQ(rows, matrix.rows());
            ASSERT_EQ(columns, matrix.columns());
        }

        void thenElementIs(double expectedValue, int rowIndex, int columnIndex) {
            ASSERT_NEAR(expectedValue, matrix(rowIndex, columnIndex), 1e-4);
        }

        void thenNormSquaredIs(double expectedValue) {
            ASSERT_NEAR(expectedValue, matrix.normSquared(), 1e-4);
        }

        void thenSumIs(double expectedValue) {
            ASSERT_NEAR(expectedValue, matrix.sum(), 1e-4);
        }
    };

    TEST_F(MatrixTest, ConstructFromGoodInitializerList) {
        givenAMatrix({{1, 2, 3},
                      {4, 5, 6}});
        thenDimensionsAre(2, 3);
        thenElementIs(1, 0, 0);
        thenElementIs(2, 0, 1);
        thenElementIs(3, 0, 2);
        thenElementIs(4, 1, 0);
        thenElementIs(5, 1, 1);
        thenElementIs(6, 1, 2);
    }

    TEST_F(MatrixTest, Transpose) {
        givenAMatrix({{1, 2, 3},
                      {4, 5, 6}});
        thenDimensionsAre(2, 3);
        whenTransposed();
        thenDimensionsAre(3, 2);
        thenElementIs(1, 0, 0);
        thenElementIs(4, 0, 1);
        thenElementIs(2, 1, 0);
        thenElementIs(5, 1, 1);
        thenElementIs(3, 2, 0);
        thenElementIs(6, 2, 1);
    }

    TEST_F(MatrixTest, MultiplyByScalar) {
        givenAMatrix({{1, 2, 3},
                      {4, 5, 6}});
        whenMultipliedBy(2);
        thenElementIs(2, 0, 0);
        thenElementIs(4, 0, 1);
        thenElementIs(6, 0, 2);
        thenElementIs(8, 1, 0);
        thenElementIs(10, 1, 1);
        thenElementIs(12, 1, 2);
    }

    TEST_F(MatrixTest, MatrixAddition) {
        givenAMatrix({{1, 2, 3},
                      {4, 5, 6}});
        whenAdded({{1, 1, 2},
                   {3, 5, 8}});
        thenElementIs(2, 0, 0);
        thenElementIs(3, 0, 1);
        thenElementIs(5, 0, 2);
        thenElementIs(7, 1, 0);
        thenElementIs(10, 1, 1);
        thenElementIs(14, 1, 2);
    }

    TEST_F(MatrixTest, MatrixMultiplication) {
        givenAMatrix({{1, 2, 3},
                      {4, 5, 6}});
        whenMultipliedOnRightBy({{7,  8},
                                 {9,  10},
                                 {11, 12}});
        thenDimensionsAre(2, 2);
        thenElementIs(58, 0, 0);
        thenElementIs(64, 0, 1);
        thenElementIs(139, 1, 0);
        thenElementIs(154, 1, 1);
    }

    TEST_F(MatrixTest, AddToEachRow) {
        givenAMatrix({{1, 2, 3},
                      {4, 5, 6},
                      {7, 8, 9}}
        );
        whenAddedToEachRow({{-1, -2, -3}});
        thenElementIs(0, 0, 0);
        thenElementIs(0, 0, 1);
        thenElementIs(0, 0, 2);
        thenElementIs(3, 1, 0);
        thenElementIs(3, 1, 1);
        thenElementIs(3, 1, 2);
        thenElementIs(6, 2, 0);
        thenElementIs(6, 2, 1);
        thenElementIs(6, 2, 2);
    }

    TEST_F(MatrixTest, SumOfRows) {
        givenAMatrix({{1, 2, 3},
                      {4, 5, 6},
                      {7, 8, 9}}
        );
        whenRowsAreSummed();
        thenDimensionsAre(1, 3);
        thenElementIs(12, 0, 0);
        thenElementIs(15, 0, 1);
        thenElementIs(18, 0, 2);
    }

    TEST_F(MatrixTest, SetRow) {
        givenAMatrix({{1, 2, 3},
                      {4, 5, 6},
                      {7, 8, 9}}
        );
        givenAnotherMatrix({{ 7, 2, 5 }});
        whenSecondRowIsSet();
        thenElementIs(7, 1, 0);
        thenElementIs(2, 1, 1);
        thenElementIs(5, 1, 2);
    }

    TEST_F(MatrixTest, NormSquared) {
        givenAMatrix({{1, 2, 3}});
        thenNormSquaredIs(14);
    }

    TEST_F(MatrixTest, NormSquared2) {
        givenAMatrix({{1, 2, 3},
                      {4, 5, 6},
                      {7, 8, 9}}
        );
        thenNormSquaredIs(285);
    }

    TEST_F(MatrixTest, Sum) {
        givenAMatrix({{1, 2, 3},
                      {4, 5, 6},
                      {7, 8, 9}}
        );
        thenSumIs(45);
    }

    TEST_F(MatrixTest, Normalize) {
        givenAMatrix({{1, 2 },
                      {3, 4 }});
        whenNormalized();
        thenElementIs(0.1, 0, 0);
        thenElementIs(0.2, 0, 1);
        thenElementIs(0.3, 1, 0);
        thenElementIs(0.4, 1, 1);
    }

}

int main(int argc, char *argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}